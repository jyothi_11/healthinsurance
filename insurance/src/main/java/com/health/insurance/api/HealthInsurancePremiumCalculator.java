package com.health.insurance.api;

import java.util.List;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.json.JSONObject;
import javax.ws.rs.core.Response;

import org.jboss.logging.Logger;

import com.health.insurance.hibernate.DatabaseInteractionImpl;
import com.health.insurance.model.CategoryDetails;

@Path("/calculator")
public class HealthInsurancePremiumCalculator {

	private static final String HEALTH_INSURANCE_PREMIUM_FOR = "Health Insurance Premium for ";
	DatabaseInteractionImpl databaseConnection = new DatabaseInteractionImpl();
	private static Double basicPremium = 5000d;

	@POST
	@Produces({ MediaType.TEXT_PLAIN })
	public Response getData(@FormParam("name") String name, @FormParam("gender") String gender,
			@FormParam("age") String age, @FormParam("currentHealth1") List<String> currentHealth,
			@FormParam("habits") List<String> habits) throws Exception {

		Double premiumCalculated = 5000d;
		Logger.getLogger(this.getClass()).info("!!!!!!!!!! Name is " + name);
//		JSONObject responseObj = new JSONObject();
		
		if (gender != null && gender.length() > 0) {
			Double percentage = databaseConnection.getPercentageByCategoryValue(gender);		
			if (percentage != null && percentage != 0) {
				premiumCalculated = premiumCalculated + (basicPremium * (percentage / 100));
			}
		}

		if (age != null && age.length() > 0) {
			Double percentage = databaseConnection.getAgeCategoryPercentage(Integer.parseInt(age));
			if (percentage != null && percentage != 0) {
				premiumCalculated = premiumCalculated + (basicPremium * (percentage / 100));
			}
		}

		if (currentHealth != null && currentHealth.size() > 0) {
			for (String health : currentHealth) {
				CategoryDetails categoryDetails = databaseConnection.getCategoryDetailsBySubCategoryName(health);
				if (categoryDetails != null && categoryDetails.getPercentage() != null) {
					Double percentage = categoryDetails.getPercentage();
					if (categoryDetails.getIsPositive()) {
						premiumCalculated = premiumCalculated - (basicPremium * (percentage / 100));
					} else {
						premiumCalculated = premiumCalculated + (basicPremium * (percentage / 100));
					}
				}
			}
		}

		if (habits != null && habits.size() > 0) {
			for (String habitsStr : habits) {
				CategoryDetails categoryDetails = databaseConnection.getCategoryDetailsBySubCategoryName(habitsStr);
				if (categoryDetails != null && categoryDetails.getPercentage() != null) {
					Double percentage = categoryDetails.getPercentage();
					if (categoryDetails.getIsPositive()) {
						premiumCalculated = premiumCalculated - (basicPremium * (percentage / 100));
					} else {
						premiumCalculated = premiumCalculated + (basicPremium * (percentage / 100));
					}
				}
			}
		}

	//	return Response.status(Response.Status.CREATED).entity("test").build();
		String output = HEALTH_INSURANCE_PREMIUM_FOR + name + ": Rs." + premiumCalculated;
		return Response.status(Response.Status.CREATED).entity(output).build();
		
		//return ;
	}

}
