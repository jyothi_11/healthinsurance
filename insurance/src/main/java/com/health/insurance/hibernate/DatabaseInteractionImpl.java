package com.health.insurance.hibernate;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;

import com.health.insurance.model.CategoryDetails;
import com.health.insurance.util.EntityManagerUtil;

public class DatabaseInteractionImpl {

	EntityManagerFactory entityManagerFactory;

	public DatabaseInteractionImpl() {
		entityManagerFactory = EntityManagerUtil.getEntityManagerFactory();
	}

	public Double getPercentageByCategoryValue(String subCategoryName) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Double percentage = null;
		try {
			percentage = (Double) entityManager
					.createQuery("select percentage from CategoryDetails cd where cd.subcategory=:subCategoryName ")
					.setParameter("subCategoryName", subCategoryName).getSingleResult();
		} catch (NoResultException e) {

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			entityManager.clear();
			entityManager.close();
		}
		return percentage;

	}

	public CategoryDetails getCategoryDetailsBySubCategoryName(String subCategoryName) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		CategoryDetails categories = null;
		try {
			System.out.println("subCategoryName " + subCategoryName);
			categories = (CategoryDetails) entityManager
					.createQuery("select cd from CategoryDetails cd where cd.subcategory=:subCategoryName")
					.setParameter("subCategoryName", subCategoryName).getSingleResult();
		} catch (NoResultException e) {

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			entityManager.clear();
			entityManager.close();
		}
		return categories;

	}

	public List<CategoryDetails> getCategoryDetailsByCategoryName(String categoryName) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		List<CategoryDetails> categories = null;
		try {
			System.out.println("entityManager " + entityManager);
			categories = entityManager
					.createQuery("select cd from CategoryDetails cd where cd.categories.categoryName=:categoryName ")
					.setParameter("categoryName", categoryName).getResultList();
		} catch (NoResultException e) {

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			entityManager.clear();
			entityManager.close();
		}
		return categories;

	}

	public Double getAgeCategoryPercentage(Integer age) {
		Double ageCategoryPercent = null;
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		try {
			System.out.println("entityManager 1 " + entityManager);
			ageCategoryPercent = (Double) entityManager
					.createQuery("SELECT SUM(ab.percentage) from AgeBoundaries ab where ab.minAge<=:age")
					.setParameter("age", age).getSingleResult();
		} catch (NoResultException e) {
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			entityManager.clear();
			entityManager.close();
		}
		return ageCategoryPercent;

	}

	public List<String> getCategoryList() {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		List<String> categoryList = null;
		try {
			categoryList = entityManager.createQuery("select categoryName from Categories c").getResultList();
		} catch (NoResultException e) {

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			entityManager.clear();
			entityManager.close();
		}
		return categoryList;

	}

}
