package com.health.insurance.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "age_boundaries")
public class AgeBoundaries {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private Integer ageBoundaryId;

	@Column(name = "min_age")
	private Integer minAge;

	@Column(name = "max_age")
	private Integer maxAge;

	@Column(name = "percentage")
	private Double percentage;

	public Integer getAgeBoundaryId() {
		return ageBoundaryId;
	}

	public void setAgeBoundaryId(Integer ageBoundaryId) {
		this.ageBoundaryId = ageBoundaryId;
	}

	public Integer getMinAge() {
		return minAge;
	}

	public void setMinAge(Integer minAge) {
		this.minAge = minAge;
	}

	public Integer getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(Integer maxAge) {
		this.maxAge = maxAge;
	}

	public Double getPercentage() {
		return percentage;
	}

	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}

}
