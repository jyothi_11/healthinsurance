package com.health.insurance.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "category_details")
public class CategoryDetails {
	@Id
	@GeneratedValue
	@Column(name = "id")
	private Integer categoryDetailsId;

	@Column(name = "sub_category_name", length = 255)
	private String subcategory;

	@OneToOne
	@JoinColumn(name = "category_id", referencedColumnName = "id")
	private Categories categories;

	@Column(name = "positive")
	private Boolean isPositive;

	public Integer getCategoryDetailsId() {
		return categoryDetailsId;
	}

	@Column(name = "percentage")
	private Double percentage;

	public void setCategoryDetailsId(Integer categoryDetailsId) {
		this.categoryDetailsId = categoryDetailsId;
	}

	public String getSubcategory() {
		return subcategory;
	}

	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;
	}

	public Categories getCategories() {
		return categories;
	}

	public void setCategories(Categories categories) {
		this.categories = categories;
	}

	public Boolean getIsPositive() {
		return isPositive;
	}

	public void setIsPositive(Boolean isPositive) {
		this.isPositive = isPositive;
	}

	public Double getPercentage() {
		return percentage;
	}

	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}

}
