package com.health.insurance.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class Users {
	@Id
	@GeneratedValue
	@Column(name = "id")
	private Integer ageBoundaryId;

	@Column(name = "login")
	private String login;

	@Column(name = "password")
	private String password;

	public Integer getAgeBoundaryId() {
		return ageBoundaryId;
	}

	public void setAgeBoundaryId(Integer ageBoundaryId) {
		this.ageBoundaryId = ageBoundaryId;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
