package com.health.insurance.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.health.insurance.hibernate.DatabaseInteractionImpl;
import com.health.insurance.model.CategoryDetails;

public class EditCategories extends HttpServlet {

	DatabaseInteractionImpl databaseConnection = new DatabaseInteractionImpl();
	public HashMap<String, List> categoryInfo = new HashMap<String, List>();

	public void doPost(HttpServletRequest httpRequest, HttpServletResponse httpResponse)
			throws IOException, ServletException {

		httpResponse.setContentType("application/json");
		
		List<String> categoryList = databaseConnection.getCategoryList();
		httpRequest.setAttribute("categoryList", categoryList);
		httpRequest.setAttribute("size", categoryList.size());
		JSONArray arrayObj=new JSONArray();
		
		for (String category : categoryList) {
			List<CategoryDetails> categoryDetailedList = databaseConnection.getCategoryDetailsByCategoryName(category);
			categoryInfo.put(category, categoryDetailedList);
		}
		System.out.println("otput " + categoryInfo.size());
		httpRequest.setAttribute("categoryDetailedList", categoryInfo);
		System.out.println("keys :" + categoryInfo.keySet() + " values " + categoryInfo.values());
		
		//arrayObj.put();
		httpResponse.getWriter().write(new JSONObject().getJSONObject(categoryInfo));
		httpRequest.getRequestDispatcher("/listCategories.jsp").forward(httpRequest, httpResponse);
	}

}
