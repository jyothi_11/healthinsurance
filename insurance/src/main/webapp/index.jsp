<html>
<head>
<script src="js/jquery.min.js"></script>       
<script>

$(document).ready(function(){

	$("#formName").submit(function(){
		
	 $.ajax({
	        type: "POST",
	        url: "health/calculator",
	        data:$( "#formName" ).serialize(),
	   //     data:{name:tname,gender:tgender,currentHealth:currentHealthArray,habits:habitsArray,age:tage},
	        success: function(data)
	        {   
		        $("#amountDisp").html(data);
	        }
	    });

	    return false;
	});
});
</script>
</head>
<body>
<div align="right"><a href="login.jsp" id="isAdmin" name="isAdmin" alngn="right">Is Admin? Click here</a></div>
<br/>
<div id="heading" align="left"><h2>Health Insurance Premium Quote Generator</h2></div>
<hr/>

<form id="formName">
<table cellspacing="5" cellpadding="5">
<tr>
	<td>Name:</td>
	<td><input type="text" name="name" id="name"/></td>
</tr>

<tr valign="top">
	<td>Gender:</td>
	<td>
		<input type="radio" id="gender" value="MALE">Male</input><br/>
		<input type="radio" id="gender" value="FEMALE">Female</input><br/>
		<input type="radio" id="gender" value="OTHER">Other</input><br/>
	</td>
</tr>
<tr>
	<td>Age:</td>
	<td><input type="text" value="" id="age" name="age"/></td>
</tr>
<tr valign="top">
	<td>Current Health:</td>
	<td>
		<input type="checkbox" id="currentHealth"  name="currentHealth1" value="Hypertension">Hypertension<br/>
		<input type="checkbox" id="currentHealth"  name="currentHealth1" value="Blood pressure">Blood pressure<br/>
		<input type="checkbox" id="currentHealth"  name="currentHealth1" value="Blood sugar">Blood sugar<br/>
		<input type="checkbox" id="currentHealth"  name="currentHealth1" value="Overweight">Overweight<br/>
	</td>
</tr>
<tr valign="top">
	<td>Habits:</td>
	<td>
		<input type="checkbox" id="habits" name="habits" value="Smoking">Smoking<br/>
		<input type="checkbox" id="habits"  name="habits"  value="Alcohol">Alcohol<br/>
		<input type="checkbox" id="habits"  name="habits"  value="Daily Exercise">Daily Exercise<br/>
		<input type="checkbox" id="habits"  name="habits"  value="Drugs">Drugs<br/>
	</td>
</tr>
<tr>
	<td colspan="2" align="center"><input type="submit" id="submitButton" name="submitButton" value="Submit"/></td>
</tr>
<tr>
<td colspan="2"><div id="amountDisp">&nbsp;</div></td>
</tr>
</table>
</form>
</body>
</html>
