<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" isELIgnored="false"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<c:choose>
<c:when test="${requestScope.size>0}">
	<table>
		<c:forEach var="category" items="${requestScope.categoryList}">
		<tr>
			<td><h3>${category}</h3></td>			
		</tr>
		<tr>
			<table>
			<c:forEach var="details" items="${requestScope.categoryDetailedList[category]}">
				<tr><td>${details.subcategory}</td><td><input type="text" value="${details.percentage}"/></td></tr>
			</c:forEach>
			</table>
		</tr>
		</c:forEach>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
				<tr><td><br/><input type="button" value="Save"/></td></tr>
	</table>	
</c:when>
<c:otherwise>
	No data
</c:otherwise>
</c:choose>

</body>
</html>	