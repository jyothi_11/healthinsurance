<html>
<head>
<script src="js/jquery.min.js"></script>       
<script>
function onSubmit(){
    var url = "http://localhost:8080/insurance/health/calculator"; 

    try{
    $.ajax({
           type: "POST",
           url: url, 
           success: function(data)
           {   
      				$("#amountDisp").innerHTML(data);
           }
         });
    }catch(e){
    	alert(e);
    }
    e.preventDefault(); 

}

</script>
</head>
<body>
<div align="right"><a href="login.jsp" id="isAdmin" name="isAdmin" alngn="right">Is Admin? Click here</a></div>
<br/>
<div id="heading" align="left"><h2>Health Insurance Premium Quote Generator</h2></div>
<hr/>

<form action="http://localhost:8080/insurance/health/calculator">
<table cellspacing="5" cellpadding="5">
<tr>
	<td>Name:</td>
	<td><input type="text" name="name"/></td>
</tr>

<tr valign="top">
	<td>Gender:</td>
	<td>
		<input type="radio" name="gender" value="MALE">Male</input><br/>
		<input type="radio" name="gender" value="FEMALE">Female</input><br/>
		<input type="radio" name="gender" value="OTHER">Other</input><br/>
	</td>
</tr>
<tr>
	<td>Age:</td>
	<td><input type="text" value="" name="age"/></td>
</tr>
<tr valign="top">
	<td>Current Health:</td>
	<td>
		<input type="checkbox" name="currentHealth" value="Hypertension">Hypertension<br/>
		<input type="checkbox" name="currentHealth" value="Blood pressure">Blood pressure<br/>
		<input type="checkbox" name="currentHealth" value="Blood sugar">Blood sugar<br/>
		<input type="checkbox" name="currentHealth" value="Overweight">Overweight<br/>
	</td>
</tr>
<tr valign="top">
	<td>Habits:</td>
	<td>
		<input type="checkbox" name="habits" value="Smoking">Smoking<br/>
		<input type="checkbox" name="habits" value="Alcohol">Alcohol<br/>
		<input type="checkbox" name="habits" value="Daily Exercise">Daily Exercise<br/>
		<input type="checkbox" name="habits" value="Drugs">Drugs<br/>
	</td>
</tr>
<tr>
	<td colspan="2" align="center"><input type="button" value="Submit" onclick="onSubmit()"></td>
</tr>
<tr>
<td colspan="2"><div id="amountDisp">&nbsp;</div></td>
</tr>

</table>
</form>
</body>
</html>
